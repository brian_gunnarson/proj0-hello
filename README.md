# Proj0-Hello

---

Trivial project to exercise version control, turn-in, and other
mechanisms.

## About:

---

Author: Brian Gunnarson

Email Address: bgunnar5@uoregon.edu

## Description:

---

This project reads a credentials file and prints the message given in the said file. For this project specifically it prints "Hello world".
